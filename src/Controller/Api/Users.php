<?php

namespace App\Controller\Api;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Users as UserEntity;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Symfony\Component\HttpFoundation\Request;

class Users extends AbstractController
{
    /**
     * EntityManager
     *
     * @var ObjectManager
     */
    private $em;

    /**
     * Récupère les informations d'une requête
     *
     * @param Request
     */
    private $request;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->em = $doctrine->getManager();
        $this->request = Request::createFromGlobals();
    }

    /**
     * Récupère un utilisateur
     * 
     * @param string $email users.email
     * @return JsonResponse
     */
    public function get(string $email): Response
    {
        $user = $this->em->getRepository(UserEntity::class)->findBy([
            'email' => $email
        ]);
        // L'utilisateur existe
        if ($user = reset($user)) {
            return new JsonResponse([
                'user' => $user
            ]);
        }
        // L'utilisateur n'existe pas
        return new JsonResponse([
            'user' => $user
        ], Response::HTTP_BAD_REQUEST);
    }
    
    /**
     * Crée un utilisateur
     * 
     * @param void
     * @return JsonResponse
     */
    public function post(): JsonResponse
    {
        // Construction de l'entité à partir des données reçues
        try {
            $user = new UserEntity();
            $user->setEmail($this->request->get('email'));
            $user->setFirstname($this->request->get('firstname'));
            $user->setLastname($this->request->get('lastname'));
            $user->setAvatar($this->request->get('avatar'));
            $user->setPassword($this->request->get('password'));
            $this->em->persist($user);
            $this->em->flush();
        } catch (Exception) {
            // Erreur en base de donnée
            return new JsonResponse([
                'user' => $user
            ], Response::HTTP_BAD_REQUEST);
        }
        // Utilisateur crée avec succès
        return new JsonResponse([
            'user' => $user
        ]);
    }

    /**
     * Modifie un utilisateur
     *
     * @param int $id users.id
     * @return JsonResponse
     */
    public function put(int $id): JsonResponse
    {
        $user = $this->em->getRepository(UserEntity::class)->find($id);

        if (null !== $this->request->get('email')) {
            $user->setEmail($this->request->get('email'));
        }
        if (null !== $this->request->get('firstname')) {
            $user->setFirstname($this->request->get('firstname'));
        }
        if (null !== $this->request->get('lastname')) {
            $user->setLastname($this->request->get('lastname'));
        }
        if (null !== $this->request->get('avatar')) {
            $user->setAvatar($this->request->get('avatar'));
        }
        if (null !== $this->request->get('password')) {
            $user->setPassword($this->request->get('password'));
        }
        
        $this->em->persist($user);
        $this->em->flush();

        return new JsonResponse([
            'user' => $user
        ]);
    }   

    /**
     * Supprime un utilisateur
     *
     * @param int $id users.id
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        $user = $this->em->getRepository(UserEntity::class)->find($id);

        // Supprime l'utilisateur
        $this->em->remove($user);
        $this->em->flush();
        // Supprime l'avatar de l'utilisateur
        try { 
            unlink('assets/images/upload/' . $user->getAvatar());
        } catch (Exception) {

        }
        
        return new JsonResponse([
            'user' => $user
        ]);
    }
}