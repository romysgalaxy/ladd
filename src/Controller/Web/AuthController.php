<?php

namespace App\Controller\Web;

use App\Entity\Users;
use App\Form\LoginType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\Form\RegisterType;
use Exception;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Component\HttpClient\TraceableHttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\Persistence\ManagerRegistry;
use App\Service\FileUploader;

class AuthController extends AbstractController
{

    /**
     * API server
     */
    const API = 'http://ladd_httpd24:80';

    /**
     * Client HTTP
     *
     * @var TraceableHttpClient
     */
    private $client;


    /**
     * Constructeur
     *
     * @param HttpClientInterface $client
     * @param ManagerRegistry $doctrine
     */
    public function __construct(HttpClientInterface $client, ManagerRegistry $doctrine)
    {
        $this->client = $client;
        $this->em = $doctrine->getManager();
    }

    /**
     * Inscription d'un utilisateur
     *
     * @param Request $request
     * @param FileUploader $fileUploader
     * @return Response
     */
    public function register(Request $request, FileUploader $fileUploader): Response
    {
        $users = new Users();
        $form = $this->createForm(RegisterType::class, $users);
        $form->handleRequest($request);

        // Formulaire soumis et validé
        if ($form->isSubmitted() && $form->isValid()) {
            // Récupère les données du formulaire
            $data = $form->getData();
            $file = $form->get('avatar')->getData();
            if ($file) {
                $path = $fileUploader->upload($file);
            }
            // Envoi de la requête à l'API
            $response = $this->client->request(
                'POST',
                self::API . '/api/users',
                [
                    'body' => [
                        'email' => $data->getEmail(),
                        'firstname' => $data->getFirstname(),
                        'lastname' => $data->getLastname(),
                        'avatar' => $path,
                        'password' => $data->getPassword(),
                    ]
                ]
            );
            try {
                // Récupère le contenu
                $result = json_decode($response->getContent());
            } catch (Exception $e) {
                // Un problème est survenu
                $error = true;
                if ($e instanceof TransportException) {
                    $message = 'Un problème est survenu, veuillez réessayer plus tard.';
                }
                if ($e instanceof ClientException) {
                    $message = 'Formulaire invalide !';
                }
                return $this->renderForm('form/register.html.twig', [
                    'form' => $form,
                    'error' => $error,
                    'message' => $message
                ]);
            }
            // Instancie la session
            $session = new Session();
            $session->set('id', $result->user->id);
            $session->set('email', $result->user->email);
            $session->set('firstname', $result->user->firstname);
            $session->set('lastname', $result->user->lastname);
            // Redirection vers le profil l'utilisateur
            return $this->redirectToRoute('admin_profile');
        }
        return $this->renderForm('form/register.html.twig', [
            'form' => $form
        ]);
    }
    
    /**
     * Connexion d'un utilisateur
     *
     * @param Request $request
     * @return Response
     */
    public function login(Request $request): Response
    {
        $users = new Users();

        $form = $this->createForm(LoginType::class, $users);
        $form->handleRequest($request);

        // Formulaire soumis et validé
        if ($form->isSubmitted() && $form->isValid()) {
            // Récupère les données du formulaire
            $data = $form->getData();
            // Envoi de la requête à l'API
            $response = $this->client->request(
                'GET',
                self::API . '/api/users/' . $data->getEmail()
            );
            try {
                // Récupère le contenu
                $result = json_decode($response->getContent());
            } catch (Exception $e) {
                // Un problème est survenu
                $error = true;
                if ($e instanceof TransportException) {
                    $message = 'Un problème est survenu, veuillez réessayer plus tard.';
                }
                if ($e instanceof ClientException) {
                    $message = 'Utilisateur introuvable !';
                }
                return $this->renderForm('form/login.html.twig', [
                    'form' => $form,
                    'error' => $error,
                    'message' => $message
                ]);
            }
            if ($this->auth($result->user->email, $result->user->password)) {
                // Instancie la session
                $session = new Session();
                $session->set('id', $result->user->id);
                $session->set('email', $result->user->email);
                $session->set('firstname', $result->user->firstname);
                $session->set('lastname', $result->user->lastname);
                // Redirection vers le profil l'utilisateur
                return $this->redirectToRoute('admin_profile');
            } else {
                $error = true;
                $message = 'Email ou mot de passe incorrect !';
                return $this->renderForm('form/login.html.twig', [
                    'form' => $form,
                    'error' => $error,
                    'message' => $message
                ]);
            }
        }
        return $this->renderForm('form/login.html.twig', [
            'form' => $form
        ]);
    }

    /**
     * Description d'un utilisateur et destruction de la session courante
     *
     * @return Response
     */
    public function logout(): Response
    {   
        $session = new Session();
        $session->clear();
        $this->addFlash(
            'success',
            'Déconnecté avec succès.'
        );
        return $this->redirectToRoute('login');
    }

    /**
     * Supprime un utilisateur
     * 
     * @param int $id users.id
     * @return Response
     */
    public function remove(int $id): Response
    {   
        // Parse l'url pour récupérer le referer
        $path = parse_url($_SERVER['HTTP_REFERER'])['path'];
        $response = $this->client->request(
            'DELETE',
            self::API . '/api/users/' . $id
        );

        try {
            // Récupère le contenu
            $result = json_decode($response->getContent());
        } catch (Exception $e) {
            // Un problème est survenu
            $this->addFlash(
                'danger',
                'Un problème est survenu, veuillez réessayer plus tard.'
            );
            // Si la suppression a lieu depuis la page /users
            if ($path === '/admin/users') {
                return $this->redirectToRoute('admin_users');
            } else {
                // Si la suppression a lieu depuis la page /profile
                return $this->redirectToRoute('admin_profile');
            }
        }

        // Si la suppression a lieu depuis la page /users
        if ($path === '/admin/users') {
            $this->addFlash(
                'success',
                'Utilisateur supprimé avec succès !'
            );
            return $this->redirectToRoute('admin_users');
        } else {
            // Si la suppression a lieu depuis la page /profile
            $this->addFlash(
                'success',
                'Votre compte a été supprimé avec succès !'
            );
            return $this->redirectToRoute('login');
        }
    }

    /**
     * Authentifie un utilisateur
     *
     * @param string $email email
     * @param string $password mot de passe
     * @return boolean
     */
    protected function auth(string $email, string $password): bool 
    {
        $user = $this->em->getRepository(Users::class)->findBy([
            'email' => $email,
            'password' => $password
        ]);

        if (isset($user[0])) {
            return true;
        }

        return false;
    }
}