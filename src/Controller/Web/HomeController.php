<?php

namespace App\Controller\Web;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class HomeController extends AbstractController
{
    /**
     * Page d'accueil
     *
     * @return Response
     */
    public function index(): Response
    {   
        $session = new Session();
        $session->start();
        
        $connected = false;
        // L'utilisateur n'est pas connecté
        if (!is_null($session->get('email'))) {
            $connected = true;
        }
        return $this->render('home.html.twig', [
            'connected' => $connected
        ]);
    }

    /**
     * Page documentation API
     *
     * @return Response
     */
    public function api(): Response
    {
        $session = new Session();
        $session->start();

        $connected = false;
        // L'utilisateur n'est pas connecté
        if (!is_null($session->get('email'))) {
            $connected = true;
        }
        return $this->render('api.html.twig', [
            'connected' => $connected
        ]);
    }
}