<?php

namespace App\Controller\Web;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Users;
use Symfony\Component\HttpFoundation\Session\Session;

class AdminController extends AbstractController
{

    /**
     * Constructeur
     *
     * @param ManagerRegistry $doctrine
     */
    public function __construct(ManagerRegistry $doctrine)
    {
        $this->em = $doctrine->getManager();
    }

    /**
     * Affiche le profil de l'utilisateur
     *
     * @return Response
     */
    public function profile(): Response
    {
        $session = new Session();
        $session->start();

        $user = $this->em->getRepository(Users::class)->find($session->get('id'));
        
        return $this->render('admin/profile.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * Affiche la liste des utilisateurs
     *
     * @return Response
     */
    public function users(): Response
    {
        $session = new Session();
        $session->start();

        $users = $this->em->getRepository(Users::class)->findAll();

        return $this->render('admin/users.html.twig', [
            'users' => $users,
            'current_user' => $this->em->getRepository(Users::class)->find($session->get('id'))
        ]);
    }
}