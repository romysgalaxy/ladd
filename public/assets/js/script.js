window.addEventListener('DOMContentLoaded', (event) => {
  console.log('DOM fully loaded and parsed');

  /** Global */
  const id_user = document.getElementById('id_user')


  /** Mobile menu */
  const MobileMenuBtn = document.getElementById('MobileMenuBtn')
  if (null !== MobileMenuBtn) {
    MobileMenuBtn.addEventListener('click', menu_func);
  }

  /** Mobile menu admin */
  const adminMobileMenuBtn = document.getElementById('adminMobileMenuBtn')
  if (null !== adminMobileMenuBtn) {
    adminMobileMenuBtn.addEventListener('click', admin_menu_func);
  }

  /** Email */
  const update_email = document.getElementById('update_email')
  if (null !== update_email) {
    const email = document.getElementById('email')
    const update_email_action = document.getElementById('update_email_action')
    const update_email_input = document.getElementById('update_email_input')
    const update_email_submit = document.getElementById('update_email_submit')
    update_email.addEventListener('click', update_email_func);
  }
  
  /** Password */
  const update_password = document.getElementById('update_password')
  if (null !== update_password) {
  const password = document.getElementById('password')
  const update_password_action = document.getElementById('update_password_action')
  const update_password_input = document.getElementById('update_password_input')
  const update_password_submit = document.getElementById('update_password_submit')
  update_password.addEventListener('click', update_password_func);
  }
});

/**
 * Mise à jour de l'adresse email
 */
function update_email_func() {

  /** Toggle */
  if (update_email_action.classList.contains('d-none')) {
    update_email_action.classList.replace('d-none', 'd-flex');
  } else {
    update_email_action.classList.replace('d-flex', 'd-none');
  }

  /** Submit */
  update_email_submit.addEventListener('click', function () {

    let value = update_email_input.value;
    let id = id_user.textContent

    if ('' !== value) {
      let xhr = new XMLHttpRequest();
      xhr.open('PUT', '/api/users/' + id, true);
      xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      xhr.onreadystatechange = () => { // Appelle la fonction quand l'état de la requête change
        if (xhr.readyState === XMLHttpRequest.DONE) {
          if (xhr.status === 200) {
            response = JSON.parse(xhr.response)
            const role_email = document.querySelectorAll('.email')
            const success_email = document.querySelector('#email .alert-success')
            // Mise à jour du DOM
            for (let i = 0; i < role_email.length; i++) {
              role_email.item(i).textContent = response.user.email
            }
            success_email.classList.replace('d-none', 'd-block')
          } else {
            const error_email = document.querySelector('#email .alert-danger')
            error_email.classList.replace('d-none', 'd-block')
          }
        }
      }
      xhr.send('email=' + value)
    }
  })
}

/**
 * Mise à jour du mot de passe
 */
 function update_password_func() {

  /** Toggle */
  if (update_password_action.classList.contains('d-none')) {
    update_password_action.classList.replace('d-none', 'd-flex');
  } else {
    update_password_action.classList.replace('d-flex', 'd-none');
  }

  /** Submit */
  update_password_submit.addEventListener('click', function () {

    let value = update_password_input.value;
    let id = id_user.textContent
    console.log(value)
    if ('' !== value) {
      let xhr = new XMLHttpRequest();
      xhr.open('PUT', '/api/users/' + id, true);
      xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      xhr.onreadystatechange = () => { // Appelle la fonction quand l'état de la requête change
        if (xhr.readyState === XMLHttpRequest.DONE) {
          if (xhr.status === 200) {
            response = JSON.parse(xhr.response)
            console.log(response)
            const success_password = document.querySelector('#password .alert-success')
            // Mise à jour du DOM
            success_password.classList.replace('d-none', 'd-block')
          } else {
            const error_password = document.querySelector('#password .alert-danger')
            error_password.classList.replace('d-none', 'd-block')
          }
        }
      }
      xhr.send('password=' + value)
    }
  })
}

function admin_menu_func() {
  const sidebar = document.getElementById('sidebar')
  /** Toggle */
  if (sidebar.style.display === 'block') {
    sidebar.style.display = 'none'
  } else {
    sidebar.style.display = 'block'
  }
}

function menu_func() {
  const navbarNav = document.getElementById('navbarNav')
  /** Toggle */
  if (navbarNav.style.display === 'block') {
    navbarNav.style.display = 'none'
  } else {
    navbarNav.style.display = 'block'
  }
}
